/// Helper trait for types that can be unwrapped
pub trait OrDefaultExt<T: Sized>: Sized {
    /// Unwraps a value, returning the default value of T if it does not exist
    fn or_default(self) -> T;
}

impl<T: Sized> OrDefaultExt<T> for Option<T>
where
    T: Default,
{
    #[inline(always)]
    fn or_default(self) -> T {
        self.unwrap_or_else(T::default)
    }
}

impl<T: Sized, E: Sized> OrDefaultExt<T> for Result<T, E>
where
    T: Default,
{
    #[inline(always)]
    fn or_default(self) -> T {
        match self {
            Ok(v) => v,
            Err(..) => T::default(),
        }
    }
}
