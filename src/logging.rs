//! Logging utility functions

use std::{
    env,
    ffi::OsStr,
    sync::Once,
};

fn env_or_default<K: AsRef<OsStr>, V: AsRef<OsStr>, F: FnOnce() -> V>(key: K, value: F) {
    let key = key.as_ref();
    if env::var_os(key).is_none() {
        env::set_var(key, value());
    }
}

static INIT_LOGGING: Once = Once::new();

/// stringified version of the default log level. `info`. Used to set environment variables when
/// `RUST_LOG` is not already present in the environment
pub const DEFAULT_LOG_LEVEL: &'static str = "info";

/// Safe version of `env_logger::init` which sets a default log level, and ensures to only call the
/// underlying `env_logger::init` method *once*
pub fn init() {
    INIT_LOGGING.call_once(|| {
        env_or_default("RUST_LOG", || format!("{}={}", env!("CARGO_CRATE_NAME"), DEFAULT_LOG_LEVEL));
        env_logger::init();
    });
}
