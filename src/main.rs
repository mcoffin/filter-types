#[macro_use] extern crate clap;
#[macro_use] extern crate log;
extern crate env_logger;
extern crate thiserror;

mod logging;
pub(crate) mod util;
mod meta_ext;

use std::{
    fs,
    io,
    str::FromStr,
    path::{
        Path,
        PathBuf,
    },
};
use meta_ext::*;

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum TypeFilter {
    File,
    Link,
    Directory,
    All,
    #[cfg(target_os = "linux")]
    Executable,
}

impl TypeFilter {
    fn check_metadata(&self, meta: &fs::Metadata) -> bool {
        match *self {
            TypeFilter::File => meta.is_file(),
            TypeFilter::Directory => meta.is_dir(),
            TypeFilter::Link => meta.is_link(),
            TypeFilter::All => true,
            #[cfg(target_os = "linux")]
            TypeFilter::Executable => meta.is_executable(),
        }
    }

    pub fn is_match<P: AsRef<Path>>(self, p: P, recursive: bool) -> io::Result<bool> {
        match (self, recursive) {
            (TypeFilter::All, _) => {
                return Ok(true);
            },
            (TypeFilter::Link, _) => {
                return fs::symlink_metadata(p).map(|meta| meta.is_link());
            },
            (_, recursive) => {
                let metadata = if recursive {
                    fs::metadata(p)
                } else {
                    fs::symlink_metadata(p)
                };
                metadata.map(|m| self.check_metadata(&m))
            },
        }
    }
}

impl Default for TypeFilter {
    fn default() -> Self {
        TypeFilter::All
    }
}

impl FromStr for TypeFilter {
    type Err = TypeFilterParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "f" | "file" => Ok(TypeFilter::File),
            "l" | "link" => Ok(TypeFilter::Link),
            "d" | "directory" => Ok(TypeFilter::Directory),
            "a" | "all"=> Ok(TypeFilter::All),
            #[cfg(target_os = "linux")]
            "e" | "executable" => Ok(TypeFilter::Executable),
            _ => Err(TypeFilterParseError::new(s)),
        }
    }
}

#[derive(Debug, thiserror::Error)]
#[error("Unknown type filter: {0}")]
#[repr(transparent)]
pub struct TypeFilterParseError(String);

impl TypeFilterParseError {
    #[inline(always)]
    pub fn new<S: Into<String>>(s: S) -> Self {
        TypeFilterParseError(s.into())
    }
}

#[derive(Debug, clap::Parser)]
#[clap(version = crate_version!(), author = crate_authors!(), about = crate_description!())]
pub struct Options {
    #[clap(long, short)]
    pub recursive: bool,
    #[clap(long, short)]
    type_filter: Vec<TypeFilter>,
    files: Vec<PathBuf>,
}

impl Options {
    #[inline(always)]
    pub fn is_recursive(&self) -> bool {
        self.recursive
    }

    #[inline]
    pub fn files<'a>(&'a self) -> &'a [PathBuf] {
        self.files.as_slice()
    }

    /// Gets an iterator over files to search. If no files were specified on the command line, the
    /// iterator will read filenames from stdin separated by newlines. If there were files
    /// specified on the command line, then stdin is ignored, and the passed-in files are used.
    pub fn files_iter<'a>(&'a self) -> FilesIterator<impl Iterator<Item=&'a PathBuf>> {
        let files = self.files();
        if files.len() <= 0 {
            FilesIterator::Stdin
        } else {
            FilesIterator::Provided(files.iter())
        }
    }

    /// Checks whether a given path meets the specified filtering requirements
    pub fn is_match<P: AsRef<Path>>(&self, p: P) -> io::Result<bool> {
        if self.type_filter.is_empty() {
            TypeFilter::default().is_match(p, self.recursive)
        } else {
            let p = p.as_ref();
            for t in &self.type_filter {
                if t.is_match(p, self.recursive)? {
                    return Ok(true);
                }
            }
            Ok(false)
        }
    }
}

pub enum FilesIterator<It: Sized> {
    Stdin,
    Provided(It),
}

impl<'a, It> Iterator for FilesIterator<It> where
    It: Iterator,
    <It as Iterator>::Item: AsRef<Path>,
{
    type Item = PathBuf;

    fn next(&mut self) -> Option<Self::Item> {
        match self {
            &mut FilesIterator::Stdin => {
                use io::BufRead;
                let stdin = io::stdin();
                let mut it = stdin.lock().lines();
                it
                    .next()
                    .and_then(Result::ok)
                    .map(PathBuf::from)
            },
            &mut FilesIterator::Provided(ref mut it) => {
                it
                    .map(|p| {
                        let p: &Path = p.as_ref();
                        PathBuf::from(p)
                    })
                    .next()
            },
        }
    }
}

fn main() {
    use clap::Parser;
    logging::init();
    let config = Options::parse();
    debug!("{:?}", &config);
    debug!("type_filter: {:?}", &config.type_filter);
    debug!("config.files.len() = {}", config.files().len());

    config.files_iter()
        .filter(|file| {
            let file_display = file.display();
            trace!("checking file: {}", &file_display);
            let ret = config.is_match(file)
                .unwrap_or_else(|e| {
                    warn!("error while checking filter for path \"{}\": {}", file.display(), &e);
                    false
                });
            trace!("{}: match = {}", &file_display, ret);
            ret
        })
        .for_each(|file| {
            println!("{}", file.display());
        });

    debug!("done");
}
