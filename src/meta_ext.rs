use std::{
    fs,
};

#[cfg(target_os = "linux")]
trait FsPermissionsExt {
    fn is_executable(&self) -> bool;
}

#[cfg(target_os = "linux")]
impl FsPermissionsExt for fs::Permissions {
    fn is_executable(&self) -> bool {
        use std::os::unix::fs::PermissionsExt;
        (self.mode() & 0o111) != 0
    }
}

/// Only valid for `fs::Metadata` instances returned from `fs::symlink_metadata`
pub trait FsMetadataExt {
    fn is_link(&self) -> bool;
    #[cfg(target_os = "linux")]
    fn is_executable(&self) -> bool;
}

impl FsMetadataExt for fs::Metadata {
    fn is_link(&self) -> bool {
        !self.is_file() && !self.is_dir()
    }

    #[cfg(target_os = "linux")]
    fn is_executable(&self) -> bool {
        let perms = self.permissions();
        self.is_file() && perms.is_executable()
    }
}
