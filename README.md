# filter-types

filters files provided either from command line args or via stdin by type.

# Usage

```
filter-types 0.1.0
Matt Coffin <mcoffin13@gmail.com>

USAGE:
    filter-types [FLAGS] [OPTIONS] [files]...

ARGS:
    <files>...

FLAGS:
    -h, --help         Prints help information
    -r, --recursive
    -V, --version      Prints version information

OPTIONS:
    -t, --type-filter <type-filter>
```

```bash
# Using find to get all files, then filtering based on executable nature
find ~ -type f -name 'asdf*' | filter-types -r -t e
# Using find to do all steps
find ~ -type f -executable -name 'asdf*'
```

# Installation and building

`filter-types` is built with [`cargo`](https://crates.io). For just building, use the following

```bash
# Add `--release` to do a non-debug build
cargo build
```

Or, you can install directly with `cargo`.

```bash
cargo install --path .
```

# License

`filter-types` is licensed under the [GNU General Public License v3.0](https://www.gnu.org/licenses/gpl-3.0.txt).
